import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Groups',
        component: () => import('../views/Groups.vue'),
    },
    {
        path: '/todos/:id',
        name: 'Todos',
        component: () => import('../views/Todos.vue'),
    },
    {
        path: '/todos/',
        redirect: '/',
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
