import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        groups: [
            { id: 0, title: 'Название группы 1', srcImg: 'https://cdn.vuetifyjs.com/images/cards/house.jpg' },
            { id: 1, title: 'Название группы 2', srcImg: 'https://cdn.vuetifyjs.com/images/cards/road.jpg' },
            { id: 2, title: 'Название группы 3', srcImg: 'https://cdn.vuetifyjs.com/images/cards/plane.jpg' },
            { id: 3, title: 'Название группы 4', srcImg: 'https://cdn.vuetifyjs.com/images/cards/road.jpg' },
            { id: 4, title: 'Название группы 5', srcImg: 'https://cdn.vuetifyjs.com/images/cards/plane.jpg' },
            { id: 5, title: 'Название группы 6', srcImg: 'https://cdn.vuetifyjs.com/images/cards/house.jpg' },
        ],
        todos: [
            {
                id: 0, groupId: 0, title: 'Какая-то задача 1', completed: true,
            },
            {
                id: 1, groupId: 0, title: 'Какая-то задача 2', completed: true,
            },
            {
                id: 2, groupId: 1, title: 'Какая-то задача 3', completed: false,
            },
            {
                id: 3, groupId: 3, title: 'Какая-то задача 4', completed: false,
            },
            {
                id: 4, groupId: 5, title: 'Какая-то задача 5', completed: false,
            },
            {
                id: 5, groupId: 5, title: 'Какая-то задача 6', completed: false,
            },
            {
                id: 6, groupId: 2, title: 'Какая-то задача 7', completed: true,
            },
            {
                id: 7, groupId: 4, title: 'Какая-то задача 8', completed: false,
            },
            {
                id: 8, groupId: 3, title: 'Какая-то задача 9', completed: false,
            },
            {
                id: 9, groupId: 1, title: 'Какая-то задача 10', completed: false,
            },
            {
                id: 10, groupId: 3, title: 'Какая-то задача 11', completed: false,
            },
            {
                id: 11, groupId: 4, title: 'Какая-то задача 12', completed: false,
            },
        ],
    },
    mutations: {
        addNewGroup(state, group) {
            state.groups.push(group);
        },
        addNewTodo(state, todo) {
            state.todos.push(todo);
        },
        deleteGroup(state, groupId) {
            const groupIndex = state.groups.findIndex(g => +g.id === +groupId);

            state.groups.splice(groupIndex, 1);
        },
        toggleTodoCompleted(state, todoId) {
            const todo = state.todos.find(t => +t.id === +todoId);

            todo.completed = !todo.completed;
        },
    },
    actions: {
        addNewGroup({ commit }, group) {
            commit('addNewGroup', group);
        },
        addNewTodo({ commit }, todo) {
            commit('addNewTodo', todo);
        },
        deleteGroup({ commit }, groupId) {
            commit('deleteGroup', groupId);
        },
        toggleTodoCompleted({ commit }, todoId) {
            commit('toggleTodoCompleted', todoId);
        },
    },
    getters: {
        allGroups: state => state.groups,
        allTodos: state => state.todos,
        nextTodoIndex: state => state.todos.length,
        nextGroupIndex: state => state.groups.length,
        getGroupById: state => groupId => state.groups.find(g => +g.id === +groupId),
        getTodosByGroupId: state => groupId => state.todos.filter(t => +t.groupId === +groupId),
        getActiveTodosByGroupId: (state, getters) => groupId => {
            const groupTodos = getters.getTodosByGroupId(groupId);

            return groupTodos.reduce((accTodoIndex, todo) => {
                if (todo.completed) {
                    return [...accTodoIndex, todo.id];
                }

                return accTodoIndex;
            }, []);
        },
    },
});
