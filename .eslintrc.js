module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/essential',
        '@vue/airbnb',
    ],
    parserOptions: {
     parser: 'babel-eslint',
    },
    rules: {
        // 0 - off, 1 - warning, 2 - error
        indent: [2, 4, { SwitchCase: 1 }],
        quotes: [2, 'single'],
        semi: [2, 'always'],
        'comma-dangle': [2, 'always-multiline'],
        'arrow-parens': [2, 'as-needed'],
        'no-debugger': 1,
        'vue/html-indent': [2, 4],
    },
};
